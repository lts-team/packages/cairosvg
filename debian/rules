#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PYVERS :=  $(shell pyversions -r)
PY3VERS := $(shell py3versions -r)

%:
	dh $@ --with python2,python3
	
override_dh_auto_build:
	set -e; \
	for py in $(PYVERS); do \
		$$py -B setup.py build -e /usr/bin/python; \
	done
	for py in $(PY3VERS); do \
		$$py -B setup.py build -e /usr/bin/python3; \
	done

override_dh_auto_clean:
	set -e; \
	for py in $(PYVERS) $(PY3VERS); do \
		$$py -B setup.py clean; \
		rm -rf build; \
	done

override_dh_auto_install:
	set -e; \
	for py in $(PYVERS); do \
		$$py -B setup.py install --skip-build --root debian/python-cairosvg \
		                      --install-layout deb; \
	done
	set -e; \
	for py in $(PY3VERS); do \
		$$py -B setup.py install --skip-build --root debian/python3-cairosvg \
		                      --install-layout deb; \
	done
	mv debian/python-cairosvg/usr/bin/cairosvg debian/python-cairosvg/usr/bin/cairosvg-py2
	mv debian/python3-cairosvg/usr/bin/cairosvg debian/python3-cairosvg/usr/bin/cairosvg-py3

override_dh_installman:
	docbook-to-man debian/manpage.cairosvg-py2.sgml > cairosvg-py2.1
	docbook-to-man debian/manpage.cairosvg-py3.sgml > cairosvg-py3.1
	dh_installman

override_dh_installchangelogs:
	dh_installchangelogs NEWS.rst
